<?php
	/**
	 * Once the user clicks the 'Login' button, verify they entered information
	 * and attempt to login using the appropriate class.
	 */
	if(isset($_POST['email']) and isset($_POST['password'])){
		switch($_POST['user-type']){
			case 'agent':
				if(Agent::login()){
					header("Location: index.php?action=agent");
				}else{
					header("Locaiton: index.php?action=login");
				}
				break;
			case 'client':
				if(Client::login()){
					header("Location: index.php?action=client");
				}else{
					header("Locaiton: index.php?action=login");
				}
				break;
		}
		return;
	}

	// Add the message from password update
	if(isset($_GET['message'])){
		$_SESSION['update_message'] = $_GET['message'];
	}
?>
<?php
	/**
	 * Display the update message if the user successfully
	 * updated their password
	 */
	if(isset($_SESSION["update_message"])){
		echo "<p id='success-message'>" . $_SESSION["update_message"] . "</p>";
		unset($_SESSION['update_message']);
	}
?>
<!-- Login Form -->
<form class="form-sign-in" method="POST" action="">
	<h2 class="form-sign-in-header">Login</h2>

	<?php if(isset($_SESSION['login_error'])) : ?>
		<div id="error-message"><?php echo $_SESSION['login_error'] ?></div>
	<?php endif; ?>
	<?php unset($_SESSION['login_error']); ?>

	<input type="text" name="email" class="form-control" placeholder="Email" required>
	
	<label for="inputPassword" class="sr-only">Password</label>
	<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
	
	<label class="radio-inline radio-left"><input type="radio" name="user-type" value="agent" checked> Agent</label>
	<label class="radio-inline radio-right"><input type="radio" name="user-type" value="client"> Client</label>

	<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
	<a class="btn btn-lg btn-primary btn-block" href="index.php?action=register">Register</a>
</form>
<!-- End Login Form-->