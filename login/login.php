<?php
	function getIsDesignatedAgent($agent_id){
		// Connect to database
		$db = DB::getInstance();
		$command = "SELECT * FROM agency WHERE designated_broker = ?";
		$stmt = $db->prepare($command);
		$query = $stmt->execute(array($agent_id));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if($result !== FALSE){
			// An agency was found where this user is the designated agent
			return TRUE;
		}else{
			// No agencies found where this user is the designated agent
			return FALSE;
		}
	}

	function verifyUser(){
		// Check if form is submitted
		if(isset($_POST['email']) and isset($_POST['password'])){
			// Verify email is valid
			if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
				try{
					$db = DB::getInstance();
					$type = $_POST['user-type'];
					if($type == 'agent'){
						$command = "SELECT * FROM agent WHERE email = ?";
						$_SESSION['type'] = 'agent';
					}else if($type == 'client'){
						$command = "SELECT * FROM client WHERE email = ?";
						$_SESSION['type'] = 'client';
					}
					$stmt = $db->prepare($command);
					$query = $stmt->execute(array($_POST['email']));
					$result = $stmt->fetch(PDO::FETCH_ASSOC);
					if($result !== FALSE){
						$storedPassword = $result['password'];
						$hash = password_verify($_POST['password'], $storedPassword);

						if($hash){
							// Successfull login
							$_SESSION['username'] = $result['firstname'] . " " . $result['lastname'];
							$_SESSION['email'] = $result['email'];
							$_SESSION['id'] = $result['id'];

							switch($type){
								case 'agent':
									$_SESSION['license'] = $result['license_number'];
									$_SESSION['is_designed_agent'] = getIsDesignatedAgent($result['id']);
									header("Location: index.php?action=agent");
									return;
									break;
								case 'client':
									$_SESSION['agent'] = $result['assigned_agent'];
									header("Location: index.php?action=client");
									return;
									break;
							}
							//header("Location: index.php?action=home");
							//return;
						}else{
							//$_SESSION['login_error'] = "Invalid password";
							$_SESSION['login_error'] = $hash;
						}
					}else{
						$_SESSION['login_error'] = "Invalid email";
					}
				} catch(PDOException $e){
					$_SESSION['db_error'] = "PDO Error: " . $e;
				}
			}else{
				$_SESSION['login_error'] = "Invalid email entry";
			}
			header("Location: index.php");
		}
	}
?>