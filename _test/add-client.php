<?php
    // Turn on error reporting
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    require_once('../connection.php');

    function clientpassword(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for($i = 0; $i < 10; $i++){
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $client_passwords[] = implode($pass);
        $string_pass =  implode($pass);
        return password_hash($string_pass, PASSWORD_DEFAULT);
    }

    function addClient(){
        $user_array = [
            '0'=>['Bob', 'Barker', 'bbarker@client.com', clientpassword(), '007'],
        ];

        $db = DB::getInstance();
        $command = "INSERT INTO client (firstname, lastname, email, password, assigned_agent, created_on) VALUES (:firstname, :lastname, :email, :password, :assigned_agent, NOW())";
        $query = $db->prepare($command);

        $count = 0;
        while($count < count($user_array)){
            $results = $query->execute(array(
                ":firstname" => $user_array[$count][0],
                ":lastname" => $user_array[$count][1],
                ":email" => $user_array[$count][2],
                ":password" => $user_array[$count][3],
                ":assigned_agent" => $user_array[$count][4],
            ));
            $count++;
        }
        echo "Client Complete";
    }

    addClient();
?>