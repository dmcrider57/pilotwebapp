<?php
    // Turn on error reporting
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    require_once('../connection.php');

    function agentpassword(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for($i = 0; $i < 10; $i++){
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $string_pass =  implode($pass);
        return password_hash($string_pass, PASSWORD_DEFAULT);
    }

    function addAgent(){
        // Add agents
        $agent_array = [
            '0'=>['Jay', 'Crider', 'jay@agent.com', agentpassword(), 'SA555464773', 'CO123456789'],
        ];

        $agent_db = DB::getInstance();
        $agent_command = "INSERT INTO agent (firstname, lastname, email, password, license_number, agency_license, created_on) VALUES (:firstname, :lastname, :email, :password, :license_number, :agency_license, NOW())";
        $query = $agent_db->prepare($agent_command);

        $agent_count = 0;
        while($agent_count < count($agent_array)){
            $results = $query->execute(array(
                ":firstname" => $agent_array[$agent_count][0],
                ":lastname" => $agent_array[$agent_count][1],
                ":email" => $agent_array[$agent_count][2],
                ":password" => $agent_array[$agent_count][3],
                ":license_number" => $agent_array[$agent_count][4],
                ":agency_license" => $agent_array[$agent_count][5],
            ));
            $agent_count++;
        }
        echo "Agent Complete";
    }

    addAgent();
?>