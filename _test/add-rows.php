<?php
    // Turn on error reporting
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    require_once('../connection.php');

    $agent_passwords = array();
    $client_password = array();

    function agentpassword(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for($i = 0; $i < 10; $i++){
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $agent_passwords[] = implode($pass);
        $string_pass =  implode($pass);
        return password_hash($string_pass, PASSWORD_DEFAULT);
    }

    function clientpassword(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for($i = 0; $i < 10; $i++){
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $client_passwords[] = implode($pass);
        $string_pass =  implode($pass);
        return password_hash($string_pass, PASSWORD_DEFAULT);
    }

    function addAgents(){
        // Add agents
        $agent_array = [
            '0'=>['Chris', 'Jordan', 'chris@agent.com', agentpassword(), 'SA123456789', 'CO123456789'],
            '1'=>['Another', 'Name', 'another@agent.com', agentpassword(), 'SA234567890', 'CO123456789'],
            '2'=>['Bob', 'Slovack', 'bob@agent.com', agentpassword(), 'SA345678901', 'CO123456789'],
            '3'=>['Charles', 'Whosit', 'charles@agent.com', agentpassword(), 'SA456789012', 'CO345678901'],
            '4'=>['Heather', 'Manner', 'heather@agent.com', agentpassword(), 'SA567890123', 'CO345678901'],
            '5'=>['Suzy', 'Worlack', 'suzy@agent.com', agentpassword(), 'SA678901234', 'CO678901234'],
            '6'=>['Bethany', 'Tribune', 'bethany@agent.com', agentpassword(), 'SA789012345', 'CO678901234'],
            '7'=>['Taylor', 'Tom', 'taylor@agent.com', agentpassword(), 'SA890123456', 'CO890123456'],
            '8'=>['Baker', 'Smith', 'baker@agent.com', agentpassword(), 'SA901234567', 'CO890123456'],
            '9'=>['Ray', 'Jackson', 'ray@agent.com', agentpassword(), 'SA012345678', 'CO789012345'],
        ];

        $agent_db = DB::getInstance();
        $agent_command = "INSERT INTO agent (firstname, lastname, email, password, license_number, agency_license, created_on) VALUES (:firstname, :lastname, :email, :password, :license_number, :agency_license, NOW())";
        $query = $agent_db->prepare($agent_command);

        $agent_count = 1;
        while($agent_count < count($agent_array)){
            $results = $query->execute(array(
                ":firstname" => $agent_array[$agent_count][0],
                ":lastname" => $agent_array[$agent_count][1],
                ":email" => $agent_array[$agent_count][2],
                ":password" => $agent_array[$agent_count][3],
                ":license_number" => $agent_array[$agent_count][4],
                ":agency_license" => $agent_array[$agent_count][5],
            ));
            $agent_count++;
        }
        echo "Agents Complete";
    }

    function addClients(){
        $user_array = [
            '0'=>['Ninette', 'Gonzales', 'ninette@client.com', clientpassword(), 'SA123456789'],
            '1'=>['Yet', 'Another', 'yet@client.com', clientpassword(), 'SA234567890'],
            '2'=>['Client', 'Test', 'client@client.com', clientpassword(), 'SA345678901'],
            '3'=>['Hope', 'Bennett', 'hope@client.com', clientpassword(), 'SA456789012'],
            '4'=>['Stave', 'Weather', 'stave@client.com', clientpassword(), 'SA567890123'],
            '5'=>['Tilly', 'Samus', 'tilly@client.com', clientpassword(), 'SA678901234'],
            '6'=>['Beauty', 'Beast', 'beauty@client.com', clientpassword(), 'SA789012345'],
            '7'=>['Someone', 'Else', 'someone@client.com', clientpassword(), 'SA890123456'],
            '8'=>['Account', 'Overload', 'account@client.com', clientpassword(), 'SA901234567'],
            '9'=>['Tetris', 'Gamey', 'tetris@client.com', clientpassword(), 'SA012345678'],
        ];

        $db = DB::getInstance();
        $command = "INSERT INTO client (firstname, lastname, email, password, assigned_agent, created_on) VALUES (:firstname, :lastname, :email, :password, :assigned_agent, NOW())";
        $query = $db->prepare($command);

        $count = 0;
        while($count < count($user_array)){
            $results = $query->execute(array(
                ":firstname" => $user_array[$count][0],
                ":lastname" => $user_array[$count][1],
                ":email" => $user_array[$count][2],
                ":password" => $user_array[$count][3],
                ":assigned_agent" => $user_array[$count][4],
            ));
            $count++;
        }
        echo "Clients Complete";
    }

    function addAgency(){
        $user_array = [
            //business_name, address_street, address_city, address_state, address_zip, designated_broker, license_number, license_type, license_status, phone_number, subscription_status
            '0'=>['COBE Real Estate, Inc.', '456 E Main St','Mesa','AZ','85213','SA123456789','CO123456789','Real Estate Corporation',1,'4803728463',2],
            '1'=>['Glenwood LLC', '890 N Westwood Lane','Mesa','AZ','85213','SA456789012','CO345678901','Real Estate Corporation',1,'6385016829',2],
            '2'=>['Focus Real Estate Group', '2345 E Camelback Rd','Mesa','AZ','85213','SA678901234','CO678901234','Real Estate Corporation',1,'2537591046',2],
            '3'=>['Cool Company Name LLC', '7192 S Thunderbird Circle','Mesa','AZ','85213','SA890123456','CO890123456','Real Estate Corporation',1,'7738299503',2],
            '4'=>['Awesome Company, Inc.', '2828 W Streetsweep Ave Ste 14','Mesa','AZ','85213','SA012345678','CO789012345','Real Estate Corporation',1,'1925546378',2],
        ];

        $db = DB::getInstance();
        $command = "INSERT INTO agency (business_name, address_street, address_city, address_state, address_zip, designated_broker, license_number, license_type, license_status, phone_number, subscription_status, created_on) VALUES (:business_name, :address_street, :address_city, :address_state, :address_zip, :designated_broker, :license_number, :license_type, :license_status, :phone_number, :subscription_status, NOW())";
        $query = $db->prepare($command);

        $count = 0;
        while($count < count($user_array)){
            $results = $query->execute(array(
                ":business_name" => $user_array[$count][0],
                ":address_street" => $user_array[$count][1],
                ":address_city" => $user_array[$count][2],
                ":address_state" => $user_array[$count][3],
                ":address_zip" => $user_array[$count][4],
                ":designated_broker" => $user_array[$count][5],
                ":license_number" => $user_array[$count][6],
                ":license_type" => $user_array[$count][7],
                ":license_status" => $user_array[$count][8],
                ":phone_number" => $user_array[$count][9],
                ":subscription_status" => $user_array[$count][10],
            ));
            $count++;
        }
        echo "Agency Complete";
    }

    function addProperty(){
        $user_array = [
            // name, address_street, address_city, address_state, address_zip, type, sub-type, class, price, sale_type, sale_notes, lot_size, number_stories, year_built, zoning_description, apn, gross_leasable_area, cap_rate
            '0'=>['Airport Business Center Executive Suites', '637 S 48th St', 'Tempe', 'AZ', '85281', 'Office', 'N/A', 'B', '0.00', 'Lease', 'Rent not disclosed', 'N/A', '1', '1980', 'N/A', 'N/A', '23261', '0.0'],
            '1'=>['Mill Avenue Business Park', '40 W Baseline Rd', 'Tempe', 'AZ', '85283', 'Office', 'N/A', 'B', '19.00', 'Lease', 'Price is per SF per Year', '8823', '1', '1984', 'N/A', 'N/A', '42880', '0.0'],
            '2'=>['Price Warner COmmerce Center', '2033 E Warner Rd', 'Tempe', 'AZ', '85284', 'Office', 'N/A', 'B', '19.75', 'Lease', 'Price is per SF per Year', '22000', '1', '1999', 'N/A', 'N/A', '22000', '0.0'],
            '3'=>['Rise on Apache', '1000 E Apache Blvd', 'Tempe', 'AZ', '85281', 'Retail', 'Apartment', 'Multifamily', '25.00', 'Lease', 'Price is per SF per Year', '2084', '1', '2017', 'N/A', 'N/A', '2084', '0.0'],
            '4'=>['Bldg D', '740 S Mill Ave', 'Tempe', 'AZ', '85281', 'Office', 'N/A', 'B', '0.00', 'Lease', 'Rent not disclosed', '1.24 AC', '1', '1997', 'N/A', 'N/A', '51035', '0.0'],
            '5'=>['Industrial Property For Sale', '7200 S Priest Dr', 'Tempe', 'AZ', '85283', 'Industrial', 'Warehouse', 'B', '5875000', 'Sale', '', '2.60 AC', '1', '1995', 'N/A', 'N/A', '48000', '0.0'],
            '6'=>['The Lofts at Orchidhouse', '21 E 6th St', 'Tempe', 'AZ', '85281', 'Multifamily', 'Apartments', 'B', '625000', 'Owner Use', '', '100000', '6', '2003', 'N/A', 'N/A', '1492', '0.0'],
            '7'=>['Office Property For Sale', '9875 S Priest Dr', 'Tempe', 'AZ', '85284', 'Office', 'N/A', 'B', '1102511', 'Investment', '', '0.50 AC', '1', '2004', 'PCC-2', '308-16-050', '5543', '7.25'],
            '8'=>['Industrial Property For Lease', '444 W 21st St', 'Tempe', 'AZ', '85282', 'Industrial', 'Warehouse', 'N/A', '6.96', 'Lease', 'Price is per SF per Year', '31200', '1', '1985', 'N/A', 'N/A', '31200', '0.0'],
            '9'=>['East Baseline Professional Plaza', '1250 E Baseline Rd', 'Tempe', 'AZ', '85283', 'Office', 'Medical', 'B', '19.00', 'Lease', 'Price is per SF per Year', '10272', '1', '1980', 'N/A', 'N/A', '10272', '0.0'],
            '10'=>['Gadzooks/ASU Submarket/Absolute Net Lease', '505 W University Dr', 'Tempe', 'AZ', '85281', 'Retail', 'Restaurant', 'B', '2430000', 'Investment', 'Investment Triple Net; Build to suit', '0.26 AC', '1', '1978', 'CSS, Tempe', '124-69-045', '2342', '5.75'],
            '11'=>['Industrial Property For Lease', '1246 E Gilbert Dr', 'Tempe', 'AZ', '85281', 'Industrial', 'Warehouse', 'N/A', '0.0', 'Lease', 'Rent not disclosed', '2739', '1', '1970', 'N/A', 'N/A', '2739', '0.0'],
            '12'=>['Residence Inn by Marriott', '125 E 5th St', 'Tempe', 'AZ', '85281', 'Hospitality', 'Hotel', 'N/A', '0.0', 'Lease', 'Rent not disclosed', '70000', '1', '2013', 'N/A', 'N/A', '70000', '0.0'],
            '13'=>['Warner Village - Bldg D', '8675 S Priest Dr', 'Tempe', 'AZ', '85284', 'Office', 'N/A', 'B', '16.00', 'Lease', 'Price is per SF per Year', '5912', '1', '2007', 'N/A', 'N/A', '5912', '0.0'],
            '14'=>['Bldg C', '7517 S McClintock Dr', 'Tempe', 'AZ', '85283', 'Office', 'Medical', 'B', '15.00', 'Lease', 'Price is per SF per Year. $15.00 to $16.85', '12803', '1', '2006', 'N/A', 'N/A', '12803', '0.0'],
        ];

        $db = DB::getInstance();
        $command = "INSERT INTO property (name, address_street, address_city, address_state, address_zip, type, sub_type, class, price, sale_type, sale_notes, lot_size, number_stories, year_built, zoning_description, apn, gross_leasable_area, cap_rate) VALUES (:name, :address_street, :address_city, :address_state, :address_zip, :type, :sub_type, :class, :price, :sale_type, :sale_notes, :lot_size, :number_stories, :year_built, :zoning_description, :apn, :gross_leasable_area, :cap_rate)";
        $query = $db->prepare($command);

        $count = 0;
        while($count < count($user_array)){
            $results = $query->execute(array(
                ":name" => $user_array[$count][0],
                ":address_street" => $user_array[$count][1],
                ":address_city" => $user_array[$count][2],
                ":address_state" => $user_array[$count][3],
                ":address_zip" => $user_array[$count][4],
                ":type" => $user_array[$count][5],
                ":sub_type" => $user_array[$count][6],
                ":class" => $user_array[$count][7],
                ":price" => $user_array[$count][8],
                ":sale_type" => $user_array[$count][9],
                ":sale_notes" => $user_array[$count][10],
                ":lot_size" => $user_array[$count][11],
                ":number_stories" => $user_array[$count][12],
                ":year_built" => $user_array[$count][13],
                ":zoning_description" => $user_array[$count][14],
                ":apn" => $user_array[$count][15],
                ":gross_leasable_area" => $user_array[$count][16],
                ":cap_rate" => $user_array[$count][17],
            ));
            $count++;
        }
        echo "Listings Complete";
    }

addProperty();
?>