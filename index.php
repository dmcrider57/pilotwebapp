<?php
    // Include the models
    require_once('_models/User.php');
    require_once('_models/Client.php');
    require_once('_models/Agency.php');
    require_once('_models/Agent.php');
    require_once('_models/Admin.php');
    //require_once('_models/Property.php');

    // Start the session
    session_start();

    // Check if we're in DEV or PROD
    $_SESSION['environment'] = 'dev';

    if($_SESSION['environment'] == 'dev'){

        require_once('dev_connection.php');
        
        // Turn on error reporting
        ini_set('display_errors',1);
        error_reporting(E_ALL);
    }else if($_SESSION['environment'] == 'prod'){

        // Include the database connection file
        require_once('connection.php');
    }
    
    if(!isset($_SESSION['username'])){
        if(isset($_GET['action'])){
            $action = $_GET['action'];
            if($action != 'login' and $action != 'register'){
                if($action != 'adminLogin' and $action != 'adminRegister'){
                    $action = 'login';
                }
            }
        }else{
            $action = 'login';
        }
    }else{
        if(isset($_GET['action'])){
            $action = $_GET['action'];
        }else{
            $action = 'logout';
        }
    }

    require_once('layout.php');
?>