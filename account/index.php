<?php
	/**
	 * If someone tries to access this page without logging in,
	 * redirect them to the login page
	 * 
	 * Otherwise, grab the user object from SESSION so we
	 * can display the various fields
	 */
	if(!isset($_SESSION['username'])){
		header('Location: index.php?action=login');
	}

	$user = $_SESSION['current_user'];


	if(file_exists($user->profile)){
		$profileImage = $user->profile;
	}else{
		$profileImage = "_public/img/profile/default_profile.jpeg";
	}

	// Display a nicer version of TRUE/FALSE for account status
	$active_state = $user->is_active;
	if($active_state == TRUE){
		$active_string = "Active";
	}else{
		$active_string = "Deactivated";
	}

	if(is_a($user, 'Client')){
		// Show None if no assigned agent
		if($_SESSION['type'] == 'client' and $user->assigned_agent === NULL){
			$assigned_agent = "Not yet assigned";
		}else{
			$assigned_agent = $user->assigned_agent;
		}
	}
?>
<div class="container container-fluid" style="font-size:1.5em;">
	<?php
		if(isset($_SESSION['error'])){
			echo "<p id='error-message'>" . $_SESSION['error'] . "</p>";
			unset($_SESSION['error']);
		}

		if(isset($_SESSION['success_message'])){
			echo "<p id='success-message'>" . $_SESSION['success_message'] . "</p>";
			unset($_SESSION['success_message']);
		}
	?>
	<div class="row">
		<div class="col-md-6">
			<p id="profile-image"><img src="<?php echo $profileImage; ?>" class="client-profile-image"></img></p>
			<p id="firstname">First name: <?php echo $user->firstname; ?></p>
			<p id="lastname">Last name: <?php echo $user->lastname; ?></p>
			<p id="email">Email: <?php echo $user->email; ?></p>
			<?php
				if($_SESSION['type'] == 'agent'){
					echo "<p id='license'>License Number: $user->license_number</p>";
					//echo "<p id='agency'>Agency Number: $user->agency_license </p>";
				}//else if($_SESSION['type'] == 'client'){
				//	echo "<p id='assigned-agent'>Assigned Agent: $assigned_agent </p>";
				//}
			?>
			<p id="created">Created On: <?php echo $user->created_on ?></p>
			<p id="status">Status: <?php echo $active_string ?></p>
			<p><a href="index.php?action=editUser" class="btn btn-lg btn-primary btn-block btn-custom">Edit Profile</a></p>
		</div>
		<div class="col-md-6">
			
		</div>
	</div>
</div>