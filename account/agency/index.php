<?php
	/**
	 * This is the page for Designated Agents to view Agency information
	 * as well as provide an option to view and edit other agents' information.
	 * 
	 * I want to improve this with AJAX
	 */
	if(!isset($_SESSION['username'])){
		header('Location: index.php?action=login');
		exit;
	}

	if(!isset($_SESSION['is_designated_broker'])){
		// Not an agent, go to My Account page
		$_SESSION['error_message'] = "You are not an Agent.";
		header('Location: index.php?action=account');
		exit;
	}else{
		if($_SESSION['is_designated_broker'] != TRUE){
			// Not a designated agent, go to My Account page
			$_SESSION['error_message'] = "You are not a Designated Broker.";
			header("Location: index.php?action=account");
			exit;
		}
	}

	$user = $_SESSION['current_user'];
?>
<!-- Display agency information -->
<div class="container container-fluid" style="font-size:1.5em;">
	<?php
		if(isset($_SESSION['success_message'])){
			echo "<p id='success-message'>" . $_SESSION['success_message'] . "</p>";
			unset($_SESSION['success_message']);
		}
		if(isset($_SESSION['error_message'])){
			echo "<p id='error-message'>" . $_SESSION['error_message'] . "</p>";
			unset($_SESSION['error_message']);
		}
	?>
	<div class="row">
		<div class="col-md-6">
			<?php
				echo Agency::outputAgencyInfo($user->agency_license);
			?>
			<p><a href="index.php?action=editAgency" class="btn btn-lg btn-primary btn-block btn-custom">Edit Agency</a></p>
		</div>
		<div class="col-md-6">
			<?php
				// Get the Agents associated with this Agency
				$agentsArray = Agency::getAllAgents($user->agency_license);
				$_SESSION['all_agents'] = $agentsArray;
				
				echo "<div class='row' style='text-decoration:underline;'><h2>Agents</h2></div>";

				foreach($agentsArray as $agent){
					$id = $agent[0];

					$agentInfo = "<div class='row'>";
					if($id == $user->id){
						$linkURL = "editUser";
					}else{
						$linkURL = "editAgents&id=" . $id;
					}
					$agentInfo .= "<a href='index.php?action=" . $linkURL . "' class='btn btn-lg btn-primary btn-block btn-custom-edit'>Edit</a>";
					$agentInfo .= "<p class='edit-text'>";
					$agentInfo .= $agent[1] . " " . $agent[2];

					if($id == $user->id){
						$agentInfo .= " (You)";
					}

					$agentInfo .= "</p>";
					$agentInfo .= "<br>";
					$agentInfo .= "</div>";

					echo $agentInfo;
				}
			?>
		</div>
	</div>
</div>