<?php
	/**
	 * This is the page for Designated Agents to update the information
	 * of other agents at their agency
	 */
	if(!isset($_SESSION['username'])){
		header('Location: index.php?action=login');
	}

	$agentID = $_GET['id'];
	$agent = Agent::getInfo($agentID);
	//$id, $fname, $lname, $email, $active, $created, $profile, $license, $agency

	// We didn't find the agent in the database, so we can't show this page
	if($agent === FALSE){
		header("Location: index.php?action=agency");
		exit();
	}

	// Make sure we have the file for the user's profile
	// If not, show the default
	if(file_exists($agent['profile'])){
		$profileImage = $agent['profile'];
	}else{
		$profileImage = "_public/img/profile/default_profile.jpeg";
	}

	// Get the pretty version of the Agent's active status
	$active_state = $agent['is_active'];
	if($active_state == TRUE){
		$status = "Active";
	}else{
		$status = "Deactivated";
	}

	// Save the changes made
	// I want to improve this using AJAX
	if(isset($_POST['save'])){
		$id = $_POST['id'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$license = $_POST['license'];
		
		$update = Agent::updateAgent($id, $firstname, $lastname, $email, $license);
		if($update === TRUE){
			$_SESSION['success_message'] = 'Agency information successfully updated.';
		}else{
			$_SESSION['error_message'] .= "There was an error updating the information. Please contact support@pilot.com for help.";
		}

		header("Location: index.php?action=agency");
		exit;
	}
?>
<div class="container container-fluid" style="font-size:2em;">
	<?php
		if(isset($_SESSION['success_message'])){
			echo "<p id='success-message'>" . $_SESSION['success_message'] . "</p>";
			unset($_SESSION['success_message']);
		}
		if(isset($_SESSION['error_message'])){
			echo "<p id='error-message'>" . $_SESSION['error_message'] . "</p>";
			unset($_SESSION['error_message']);
		}
	?>
	<div class="row">
	<div class="col-md-12">
			<form class="edit-info" method="POST" action="">
				<h2>Editing - <?php echo $agent['firstname'] . " " . $agent['lastname']; ?></h2>
				<p class="note">If you wish to update this agent's account status or affiliation with your agency, please contact the Pilot admins at <a href="mailto:accounts@pilot.com">accounts@pilot.com</a></p>
				<input name="id" value="<?php echo $agent['id']; ?>" hidden>
				<p id="profile-image"><img src="<?php echo $profileImage; ?>" class="client-profile-image"></img></p>
				<p>Only the individual Agent can update their profile image at this time.</p>
				<br>
				<label for="firstname">First Name:</label><input id="firstname" name="firstname" value="<?php echo $agent['firstname']; ?>">
				<br>
				<label for="lastname">Last Name:</label><input id="lastname" name="lastname" value="<?php echo $agent['lastname']; ?>">
				<br>
				<label for="email">Email:</label><input id="email" name="email" value="<?php echo $agent['email']; ?>">
				<br>
				<label for="license">License Number:</label><input id="license" name="license" value="<?php echo $agent['license_number']; ?>">
				<br>
				<label for="agency">Agency's License:</label><input id="license" name="license" value="<?php echo $agent['agency_license']; ?>" readonly>
				<br>
				<label for="status">Pilot Account Status:</label><input id="status" name="status" value="<?php echo $status; ?>" readonly>
				<br>
				<button class="btn btn-lg btn-primary btn-custom-save" type="submit" name="save">Save Changes</button>
			</form>
		</div>
	</div>
</div>