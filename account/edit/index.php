<?php
	/**
	 * This page allows a user to change their password and profile image
	 */
	if(!isset($_SESSION['username'])){
		header('Location: index.php?action=login');
	}

	$user = $_SESSION['current_user'];
	if(file_exists($user->profile)){
		$profileImage = $user->profile;
	}else{
		$profileImage = "_public/img/profile/default_profile.jpeg";
	}

	// Change password
	if(isset($_POST['current_password']) and isset($_POST['new_password']) and isset($_POST['changePassword'])){
		if(User::updatePassword($_POST['current_password'], $_POST['new_password'], $_SESSION['type'])){
			// Update was successful
			$_SESSION['update_message'] = 'Password Updated successfully. Please login with the new password to continue.';
			header("Location: index.php?action=logout");
			exit;
		}else{
			// Update failed
			$_SESSION['error_message'] = $_SESSION['message'];
			header("Location: index.php?action=editUser");
			exit;
		}
	}

	// Change profile image
	if(isset($_POST['submit_image'])){
		try{
			$message = "";
			$target_dir = "_public/img/profile/";
			$extension = end(explode(".",$_FILES['fileToUpload']['name']));
			$target_name = $user->id . ".";
			$target_file = $target_dir . $target_name . $extension;
			$_SESSION['file_name'] = $target_file;
			$uploadOk = 1;
			// Check for fake images
			$check = getimagesize($_FILES['fileToUpload']['tmp_name']);
			if($check !== false){
				// Check for file types
				if($extension != 'jpg' and $extension != 'png' and $extension != 'jpeg'){
					$uploadOk = 0;
					$message .= "Wrong file type: " . $extension;
				}else{
					$uploadOk = 1;
				}
			}else{
				// Fake - don't upload
				$uploadOk = 0;
				$message .= "Not a valid image filetype.";
			}

			if($uploadOk == 1){
				$message = "SUCCESS";
				move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$target_file);

				// Save it as the user's new profile image
				User::updateProfileImage($target_file, $user->id, $_SESSION['type']);

				$_SESSION['success_message'] = "Successfully updated profile image.";
				header('Location: index.php?action=editUser');
				exit;
			}else{
				// Upload failed
				$_SESSION['error_message'] = $message;
				header('Location: index.php?action=editUser');
				exit;
			}
		}catch(Exception $e){
			$_SESSION['error-message'] = "ERROR: " . $e;
			header('Location: index.php?action=editUser');
			exit;
		}
	}
?>
<div class="container container-fluid" style="font-size:1.5em;">
	<?php
		if(isset($_SESSION['error_message'])){
			echo "<p id='error-message'>" . $_SESSION['error_message'] . "</p>";
			unset($_SESSION['error_message']);
		}

		if(isset($_SESSION['success_message'])){
			echo "<p id='success-message'>" . $_SESSION['success_message'] . "</p>";
			unset($_SESSION['success_message']);
		}
	?>
	<div class="row">
		<div class="col-md-6">
			<form class="form-sign-in" method="POST" action="">
				<h3>Change Password</h3>

				Current Password: <input type="password" name="current_password" id="inputCurrentPassword" class="form-control" required>

				<br>

				New password: <input type="password" name="new_password" id="inputNewPassword" class="form-control" placeholder="Password" required>

				<input name="changePassword" class="btn btn-lg btn-primary btn-block" type="submit" value="Change Password">
			</form>
		</div>
		<div class="col-md-6">
			<form class="form-sign-in" method="POST" action="" enctype="multipart/form-data">
				<h3>Change Profile Image</h3>

				<p id="profile-image"><img src="<?php echo $profileImage; ?>" class="client-profile-image"></img></p>

				<span style="font-size:0.75em;">
					Select image to upload. Only JPEG, JPG, and PNG file types are allowed.
					<input type="file" name="fileToUpload" id="fileToUpload">
					<br>
					<input type="submit" value="Upload Image" name="submit_image" class="btn btn-lg btn-primary btn-block">
				</span>
			</form>
		</div>
	</div>
</div>