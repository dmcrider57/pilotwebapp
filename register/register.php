<?php
	function getUserID($email, $type){
		$db = DB::getInstance();
		
		if($type == 'agent'){
			$command = "SELECT * FROM agent WHERE email = ?";
		}else if($type == 'client'){
			$command = "SELECT * FROM client WHERE email = ?";
		}

		$stmt = $db->prepare($command);
		$query = $stmt->execute(array($email));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if($result !== FALSE){
			// We just created the user, so all we need is the id
			return $result['id'];
		}
	}
	// Check if form is submitted
	function addUser(){
		if(isset($_POST['firstname']) and isset($_POST['lastname']) and isset($_POST['password'])){
			// Get all the fields
			$firstname = stripslashes($_POST['firstname']);
			$lastname = stripslashes($_POST['lastname']);
			$password = $_POST['password'];
			$password2 = $_POST['password2'];
			$email = $_POST['email'];
			
			if($password === $password2 && filter_var($email, FILTER_VALIDATE_EMAIL)){
				
				// Hash the password for security
				$hash = password_hash($password, PASSWORD_DEFAULT);
				
				try{
					// Initiate a connection to the database
					$db = DB::getInstance();

					$type = $_POST['user-type'];
					if($type == 'agent'){
						$license = $_POST['license'];
						$agency = $_POST['agency-license'];

						$command = "INSERT INTO agent(firstname, lastname, email, password, license_number, agency_license, created_on) VALUES (:firstname, :lastname, :email, :password, :license_number, :agency_license, NOW());";
						$query = $db->prepare($command);
						$results = $query->execute(array(":firstname" => $firstname, ":lastname" => $lastname, ":email" => $email, ":password" => $hash, ":license_number" => $license, ":agency_license" => $agency));
						// Set the header
						$header = "Location: index.php?action=agent";
						exit;
					}else if($type == 'client'){
						$agent = $_POST['agent'];

						$command = "INSERT INTO client(firstname, lastname, email, password, assigned_agent, created_on) VALUES (:firstname, :lastname, :email, :password, :assigned_agent, NOW());";
						$query = $db->prepare($command);
						$results = $query->execute(array(":firstname" => $firstname, ":lastname" => $lastname, ":email" => $email, ":password" => $hash, ":assigned_agent" => $agent));
						// Set the header
						$header = "Location: index.php?action=client";
						exit;
					}

					$_SESSION['username'] = $firstname . " " . $lastname;
					$_SESSION['email'] = $email;
					$_SESSION['id'] = getUserID($email, $type);
					header($header);
					exit;
				} catch (PDOException $e){
					$_SESSION['db_error'] = "PDO Error: " . $e;
				}
			}else{
				$_SESSION['register_error'] = "Something went wrong. Please try again later.";
			}
		}else{
			$_SESSION['register_error'] = "Something went wrong. Please try again later.";
		}
	}

	function doesUserExist(){
		try{
			$type = $_POST['user-type'];
			$db = DB::getInstance();
			if($type == 'agent'){
				$command = "SELECT * FROM agent WHERE email = ?";
			}else if($type == 'client'){
				$command = "SELECT * FROM client WHERE email = ?";
			}
			$stmt = $db->prepare($command);
			$result = $stmt->execute([$_POST['email']]);
			if($result){
				$users = $stmt->fetchAll();
			}else{
				return false;
			}
		}catch(PDOException $e){
			$_SESSION['db_error'] = "PDO Error: " . $e;
		}

		if(!isset($users[0])){
			// No user with that email exists
			addUser();
		}else{
			// Email already exists - cannot add user
			$_SESSION['register_error'] = "An account for that email already exists.";
			header("Location: index.php?action=register");
		}
	}
?>