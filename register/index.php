<?php
	//session_start();
	require("register.php");

	if(isset($_POST['lastname'])){
		if(doesUserExist()){
			echo addUser();
		}
		return;
	}
?>
<script>
	$(function(){
		$(".client").hide();
		$('input[type="radio"]').click(function(){
			var inputValue = $(this).attr("value");
			var targetBox = $("." + inputValue);
			console.log(inputValue);
			console.log(targetBox);
			$(".type").not(targetBox).hide();
			$(targetBox).show();
		});
	});
</script>
<!-- Registration form -->
<form class="form-sign-in" method="POST" action="">
	<h2 class="form-sign-in-header">Registration</h2>

	<?php if(isset($_SESSION['register_error'])) : ?>
		<div id="error-message"><?php echo $_SESSION['register_error'] ?></div>
	<?php endif; ?>
	<?php unset($_SESSION['register_error']); ?>
	
	<input type="text" name="firstname" class="form-control" placeholder="First name" required>
	<input type="text" name="lastname" class="form-control" placeholder="Last name" required>
	
	<input type="email" name="email" class="form-control" placeholder="Email" required>
	
	<input type="text" name="license" class="form-control agent type" placeholder="My License Number" id="agent">
	<input type="text" name="agency-license" class="form-control agent type" placeholder="Agency License Number" id="agent">
	<input type="text" name="agent" class="form-control client type" placeholder="Agent License Number" id="client">

	<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

	<input type="password" name="password2" id="verifyPassword" class="form-control" placeholder="Verify Password" required>

	<label class="radio-inline radio-left"><input type="radio" name="user-type" value="agent" checked> Agent</label>
	<label class="radio-inline radio-right"><input type="radio" name="user-type" value="client"> Client</label>

	<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
	<a class="btn btn-lg btn-primary btn-block" href="index.php?action=login">Login</a>
</form>