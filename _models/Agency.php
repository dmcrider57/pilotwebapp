<?php

class Agency{
    public $id;
    public $business_name;
    public $address_street;
    public $address_city;
    public $address_state;
    public $address_zip;
    public $designated_broker;
    public $license_number;
    public $license_type;
    public $license_status;
    public $phone_number;
    public $subscription_status;
    public $is_active;
    public $created_on;

    public function __construct($id,$business_name,$address_street,$address_city,$address_state,$address_zip,$designated_broker,$license_number,$license_type,$license_status,$phone_number,$subscription_status,$is_active,$created_on){
        $this->id = $id;
        $this->business_name = $business_name;
        $this->address_street = $address_street;
        $this->address_city = $address_city;
        $this->address_state = $address_state;
        $this->address_zip = $address_zip;
        $this->designated_broker = $designated_broker;
        $this->license_number = $license_number;
        $this->license_type = $license_type;
        $this->license_status = $license_status;
        $this->phone_number = $phone_number;
        $this->subscription_status = $subscription_status;
        $this->is_active = $is_active;
        $this->created_on = $created_on;
    }

    /**
     * Determine if an Agent is the Designated Broker
     * 
     * @param int $id is the ID of the Agent we want to check
     * 
     * @return bool True if Agent is the Designated Broker, else FALSE
     */
    public static function isDesignatedBroker($agentLicense){
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agency WHERE designated_broker = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($agentLicense));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Agent is the Designated Broker
                return TRUE;
            }else{
                // Agent is NOT the Designated Broker
                return FALSE;
            }
        }catch(PDOException $e){
            $_SESSION['error'] = "Database error:  " . $e;
            return FALSE;
        }
    }

    /**
     * Returns select information about the agency defined by $agencyLicense
     * 
     * @param string $agencyLicense is the license of the agency we want more info about
     * 
     * @return array of agency data
     */
    public static function getAgencyInfo($agencyLicense){
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agency WHERE license_number = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($agencyLicense));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Agency was found. Return all information
                // id, business_name, address_street, address_city, address_state, address_zip, designated_broker*, license_number, license_type*, license_status, phone_number, subscription_status, is_active, created_on*
                // * = not returned
                return array($result['id'],$result['business_name'],$result['address_street'],$result['address_city'],$result['address_state'],$result['address_zip'],$result['license_number'],$result['license_status'],$result['phone_number'],$result['subscription_status'],$result['is_active']);

            }else{
                // No Agnecy found. That's bad.
                $_SESSION['error_message'] = "No Agency found with License Number " . $agencyLicense;
                return NULL;
            }
        }catch(PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return NULL;
        }
    }

    /**
     * Gets all agents associated with the defined agency
     * 
     * @param string $agencyLicense is the agency whose agents we want
     * 
     * @return array $agents contains array of Agent objects
     */
    public static function getAllAgents($agencyLicense){
        $agents = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agent WHERE agency_license = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($agencyLicense));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Return all Agents found
                $agents = [];
                foreach($result as $row){
                    $data = [];
                    // Get relevant info
                    array_push($data, $row['id']);
                    array_push($data, $row['firstname']);
                    array_push($data, $row['lastname']);
                    array_push($data, $row['email']);
                    array_push($data, $row['license_number']);
                    array_push($data, $row['is_active']);
                    // Nest the array
                    array_push($agents,$data);
                }
                // Return the nested arrays
                return $agents;
            }else{
                // No Agnecy found. That's bad.
                $_SESSION['error_message'] = "No Agency found with License Number " . $agencyLicense;
                return $agents;
            }
        }catch(PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $agents;
        }
    }

    /**
     * 'Pretty print' of an agency's information
     * 
     * @param string $agencyLicense is license of agency whose info we want to display
     * 
     * @return string $output is the HTML and data we want to display
     */
    public static function outputAgencyInfo($agencyLicense){
        $output = "";
        $agency = Agency::getAgencyInfo($agencyLicense);

        switch($agency[9]){
            case 0:
                $subscription_status = "Not Active";
                break;
            case 1:
                $subscription_status = "3 Month Trial";
                break;
            case 2:
                $subscription_status = "Contract - 1 Year";
                break;
            case 3:
                $subscription_status = "Contract - 3 Year";
                break;
        }

        // Display Agency information
        $output .= "<h2 style='text-decoration:underline;'>Agency Information</h2>";
        $output .= "<p id='agency_name'>$agency[1]</p>";
        $output .= "<p id='agency_address'>$agency[2] $agency[3], $agency[4] $agency[5]</p>";
        $output .= "<p id='agency_phone'>Phone Number: $agency[8]</p>";
        $output .= "<p id='agency_license'>License: $agency[6]</p>";
        $output .= "<p id='agency_subscription'>Subscription: $subscription_status</p>";


        return $output;
    }

    /**
     * Updates the database record for the agency whose $id is passed in. SHould really look
     * into using AJAX instead.
     * 
     * @param int $id of the agency we want to update
     * @param string $name of the agency
     * @param string $street address of the agency
     * @param string $city of the agency
     * @param string $state of the agency
     * @param string $zip code of the agency
     * @param int $phone number of the agency
     * @param string $license number of the agency
     */
    public static function updateInfo($id, $name, $street, $city, $state, $zip, $phone, $license){
        $db = DB::getInstance();
        $command = "SELECT * FROM agency WHERE id = ?";
        $stmt = $db->prepare($command);
        $query = $stmt->execute(array($id));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if($result !== FALSE){
            try{            
				$update_db = DB::getInstance();
				$update_command = "UPDATE agency SET business_name=:bname, address_street=:st, address_city=:city, address_state=:state, address_zip=:zip, phone_number=:phone, license_number=:license WHERE id=:id";
				$update_stmt = $update_db->prepare($update_command);
            	$update_result = $update_stmt->execute(array(":bname"=>$name, ":st"=>$street, ":city"=>$city, ":state"=>$state, ":zip"=>$zip, ":phone"=>$phone, ":license"=>$license, ":id"=>$id));
            
				if($update_result !== FALSE){
					return TRUE;
				}else{
					$_SESSION['error_message'] = "Information Update error";
					return FALSE;
				}
				
			}catch(PDOException $e){
				$_SESSION['error_message'] = "There was an error: " . $e;
				return FALSE;
			}
        }
    }
}
?>