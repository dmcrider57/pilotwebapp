<?php
/**
 * This class gets information from the database specific to Clients.
 * It extends the User class which gets information from the
 * database common to all users.
 * 
 * Clients have the following unique property:
 *      assigned_agent  varchar(15) -> reference to `license_number` field of associated agent
 */
class Client extends User{
    
    public $assigned_agent;

    /**
     * Creates an Client object
     * 
     * @param User $user is the User object returned by calling User::login() below
     * @param string $agent is the license number of the Agent
     * 
     * @return void
     */
    public function __construct($user, $agent){
        parent::__construct($user[0], $user[1], $user[2], $user[3], $user[4], $user[5], $user[6]);
        $this->assigned_agent = $agent;
    }
    
    /**
     * Called from /login/index.php
     * 
     * Validates the login information and sets the $_SESSION information
     * if the login is successful
     * 
     * @return bool
     */
    public static function login(){
        $_SESSION['type'] = 'client';

        // Initiate a database connection
        $db = DB::getInstance();
        $command = "SELECT * FROM client WHERE email = ?";

        // Use the User class to get the common fields from the database
        $login_array = User::login($db, $command);

        if($login_array !== FALSE){
            // This contains the base User information
            $user = $login_array[0];
            // This lets us access Client-unique columns from the table
            $result = $login_array[1];

            $user = new Client($user, $result['assigned_agent']);
            $_SESSION['current_user'] = $user;

            // Most views need a username to be set in order to be seen
            $_SESSION['username'] = $user->firstname . " " . $user->lastname;

            // Return that the login was successful
            return TRUE;
        }else{
            // Unable to login
            return FALSE;
        }
    }

    /**
     * Gets a client object
     * 
     * @param int $id is the unique id of the client
     * 
     * @return Client Returns a Client object
     */
    public static function getClient($id){
        $db = DB::getInstance();
        $command = "SELECT * FROM client WHERE id = ?";
        $stmt = $db->prepare($command);
        $query = $stmt->execute(array($id));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $return_array = [];

        if($result !== FALSE){
            // Client found
            foreach($result as $item){
                array_push($return_array, $item);
            }
            return $return_array;
        }else{
            // Client not found
            return NULL;
        }
    }

    /**
     * Refreshes the cached user when updates are made that don't require a logout
     * @param int $id is the id of the user whose record we are updating
     */
    public static function refreshClient($id){
        $result = User::refreshUser($id, 'client');
        $newUser = $result[0];
        $agent = $result[1]['assigned_agent'];
        $_SESSION['current_user'] = new Client($newUser,$agent);
    }

    /**
     * Overrides the default __toString()
     * 
     * @return string
     */
    public function __toString(){

        $output = "Name: " . $this->firstname;
        $output .= "<br>";
        $output .= "Assigned Agent: " . $this->assigned_agent;
        $output .= "<br>";
        $output .= "Profile link: " . $this->profile;

        return $output;
    }
}
?>