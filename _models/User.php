<?php
/**
 * This class gets information from the database common to
 * both Agents and Clients
 * 
 * Users have the following common properties:
 *      id              int(11)
 *      firstname       varchar(50)
 *      lastname        varchar(50)
 *      email           varchar(50)
 *      password        varchar(255)
 *      is_active       true/false  -> default TRUE
 *      created_on      datetime
 */
class User{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $is_active;
    public $created_on;
    public $profile;

    /**
     * Creates a User object with properties common to Agents and Clients
     * 
     * @param string $id is the unique key from the database auto-assigned on account creation
     * @param string $fname is the user's firstname
     * @param string $lname is the user's lastname
     * @param string $email is the user's email address. This is used to identify clients and is unique
     * @param string $active indicates if teh user's account is active or not
     * @param datetime $created is the timestamp from when the user's account was created.
     * @param string $profile is the name of the user's profile image.
     * 
     * @return void
     */
    public function __construct($id, $fname, $lname, $email, $active, $created, $profile){
        $this->id = $id;
        $this->firstname = $fname;
        $this->lastname = $lname;
        $this->email = $email;
        $this->is_active = $active;
        $this->created_on = $created;
        $this->profile = $profile;
    }

    /**
     * Called by both Agent and Client classes to verify a login attempt
     * 
     * @param DB $db is the database instance that will be used
     * @param string $command is the SQL query that will be used
     * 
     * @return bool TRUE for successful login, FALSE for failed login
     */
    public static function login($db, $command){
        try{
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($_POST['email']));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                $storedPassword = $result['password'];
                $hash = password_verify($_POST['password'], $storedPassword);

                if($hash){
                    // Successfull login
                    // Return the fields common to all users as well as the $result from the query
                    // so that the Agent and Client classes can get the other values unique to them
                    if($result['profile'] === NULL){
                        $result['profile'] = '_public/img/profile/default_profile.jpeg';
                    }

                    return array(array($result['id'], $result['firstname'], $result['lastname'], $result['email'], $result['is_active'], $result['created_on'], $result['profile']), $result);
                }else{
                    $_SESSION['login_error'] = "Invalid password";
                    return FALSE;
                }
            }else{
                $_SESSION['login_error'] = "Invalid email";
                return FALSE;
            }
        } catch(PDOException $e){
            // Database error
            $_SESSION['db_error'] = "PDO Error: " . $e;
            return FALSE;
        } 
    }

    /**
     * Allows the user to update their password
     * 
     * @param string $old is the old password
     * @param string $new is the new password
     * @param string $table identifies if we should use the 'agent' or 'client' table
     * 
     * @return bool TRUE if update was successful
     */
    public static function updatePassword($old, $new, $table){
        $user = $_SESSION['current_user'];

        $db = DB::getInstance();
        $command = "SELECT * FROM $table WHERE email= ?";
        $stmt = $db->prepare($command);
        $query = $stmt->execute(array($user->email));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if($result !== FALSE){
            $storedPassword = $result['password'];
            $hash = password_verify($old, $storedPassword);
            if($hash){
                try{
                    $userid = $result['id'];
                            $update_hash = password_hash($new, PASSWORD_DEFAULT);
                
                    $update_db = DB::getInstance();
                    $update_command = "UPDATE $table SET password=:newpassword WHERE id=:userid";
                    $update_stmt = $update_db->prepare($update_command);
                    $update_result = $update_stmt->execute(array(":newpassword" => $update_hash, ":userid" => $userid));
                
                    if($update_result !== FALSE){
                        return TRUE;
                    }else{
                        $_SESSION['message'] = "Password Update error";
                        return FALSE;
                    }
                    
                }catch(PDOException $e){
                    $_SESSION['message'] = "There was an error: " . $e;
                    return FALSE;
                }
            }else{
                $_SESSION['message'] = 'Current password is not valid.';
                return FALSE;
            }
        }else{
            $_SESSION['message'] = "No results for Email: ". $user->email;
            return FALSE;
        }
    }
    /**
     * Allows users to update their profile image
     * 
     * @param string $newImage is the path to the new image
     * @param int $userID is the ID of the user whose image we are changing
     * @param string $userType determines if we use the client or agent table
     * 
     */
    public static function updateProfileImage($newImage, $userID, $userType){
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM $userType WHERE id = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($userID));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Update the image with the new one and save it
                try{
                    $result['profile'] = NULL;
                    $updateDB = DB::getInstance();
                    $updateCommand = "UPDATE $userType SET profile='$newImage' WHERE id = ?";
                    $updateStmt = $updateDB->prepare($updateCommand);
                    $updateQuery = $updateStmt->execute(array($userID));

                }catch(PDOException $e){
                    $_SESSION['db_error'] = "Database error: " . $e;
                }
            }
        }catch(PDOException $e){
            $_SESSION['db_error'] = "Database error: " . $e;
        }
    }

    /**
     * Updates the cached user for changes that don't require a logout
     * @param int $id is the id of the user whose record we are updating
     * @param string $type determines if we're updating a client or an agent
     * 
     * @return array of all the values we need to essentially re-login without actually doing it
     */
    public static function refreshUser($id, $type){
        $db = DB::getInstance();
        $command = "SELECT * FROM $type WHERE id = ?";
        $stmt = $db->prepare($command);
        $query = $stmt->execute(array($userID));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if($result !== FALSE){
            return array(array($result['id'], $result['firstname'], $result['lastname'], $result['email'], $result['is_active'], $result['created_on'], $result['profile']), $result);
        }else{
            return NULL;
        }
    }
}
?>