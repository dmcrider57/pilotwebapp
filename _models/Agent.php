<?php
/**
 * This class gets information from the database specific to Agents.
 * It extends the User class which gets information from the
 * database common to all users.
 * 
 * Agents have the following unique properties:
 *      license_number    varchar(15)
 *      agency_license    varchar(15) -> reference to `license_number` field of associated agency
 */
class Agent extends User{
    
    public $license_number;
    public $agency_license;

    /**
     * Creates an Agent object
     * 
     * @param User $user is the User object returned by calling User::login() below
     * @param string $license is the license number of the Agent
     * @param string $agency is the license number of the Agent's agency
     * 
     * @return void
     */
    public function __construct($user, $license, $agency){
        parent::__construct($user[0], $user[1], $user[2], $user[3], $user[4], $user[5], $user[6]);
        $this->license_number = $license;
        $this->agency_license = $agency;
    }
    
    /**
     * Called from login/index.php
     * 
     * Validates the login information and sets the $_SESSION information
     * if the login is successful
     * 
     * @return bool
     */
    public static function login(){
        $_SESSION['type'] = 'agent';

        // Initiate a database connection
        $db = DB::getInstance();
        $command = "SELECT * FROM agent WHERE email = ?";

        // Use the User class to get the common fields from the database
        $login_array = User::login($db, $command);

        if($login_array !== FALSE){
            // This contains the base User information
            $user = $login_array[0];
            // This lets us access Agent-unique columns from the table
            $result = $login_array[1];

            $user = new Agent($user, $result['license_number'], $result['agency_license']);
            $_SESSION['current_user'] = $user;

            // Most views need a username to be set in order to be seen
            $_SESSION['username'] = $user->firstname . " " . $user->lastname;

            // Determine if the current Agent is a Designated Agent
            $_SESSION['is_designated_broker'] = Agency::isDesignatedBroker($user->license_number);

            // Return that the login was successful
            return TRUE;
        }else{
            // Unable to login
            return FALSE;
        }
    }

    /**
     * Returns the name and database id of each client associated with the agent
     * 
     * @param Agent $agent is an Agent object
     * 
     * @return array $clients where each index in the array has the name and id of a client
     */
    public static function getClients($agent){
        $clients = array();

        $db = DB::getInstance();
        $command = "SELECT * FROM client WHERE assigned_agent = ?";
        $stmt = $db->prepare($command);
        $query = $stmt->execute(array($agent->license_number));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($result !== FALSE){
            // At least one Client found
            foreach($result as $record){
                $clients[] = array($record['id'], $record['firstname'] . " " . $record['lastname']);
            }
            return $clients;
        }else{
            // No clients found
            return NULL;
        }
    }

    /**
     * Refreshes the cached user when updates are made that don't require a logout
     * @param int $id is the id of the user whose record we are updating
     */
    public static function refreshClient($id){
        $result = User::refreshUser($id, 'agent');
        $newUser = $result[0];
        $license = $result[1]['license_number'];
        $agency = $result[1]['agency_license'];
        $_SESSION['current_user'] = new Agent($newUser,$license, $agency);
    }

    /**
     * Queries the database for an agent's information
     * 
     * @param int $id of agent whose info we want
     * 
     * @return array $agent is the row of the database that matches the $id
     */
    public static function getInfo($id){
        $agent = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agent WHERE id = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($id));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Agent was found. Return an array with all the values.
                $agent = $result;
                return $agent;
            }else{
                // Agent does not exist in the database. That's a big error
                $_SESSION['error_message'] = "Agent does not exist in the database.";
                return FALSE;
            }
        }catch(PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $agent;
        }
    }

    /**
     * Updates the agent information
     * 
     * @param int $id of the agent we want to update
     * @param string $firstname of the agent
     * @param string $lastname of the agent
     * @param string $email of the agent
     * @param string $license number of the agent
     * 
     * @return bool TRUE if update was successful, else FALSE
     */
    public static function updateAgent($id, $firstname, $lastname, $email, $license){
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agent WHERE id = ?";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array($id));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result !== FALSE){
                // Agent was found. Let's update.
                try{
                    $update_db = DB::getInstance();
                    $update_command = "UPDATE agent SET firstname=:fname, lastname=:lname, email=:email, license_number=:license WHERE id=:id";
                    $update_stmt = $update_db->prepare($update_command);
                    $update_result = $update_stmt->execute(array(":fname"=>$firstname, ":lname"=>$lastname, ":email"=>$email, ":license"=>$license, ":id"=>$id));
                
                    if($update_result !== FALSE){
                        return TRUE;
                    }else{
                        $_SESSION['error'] = "Information Update error";
                        return FALSE;
                    }
                }catch(PDOException $e){
                    $_SESSION['error'] = "Database error: " . $e;
                    return FALSE;
                }
            }else{
                // Agent does not exist in the database. That's a big error
                $_SESSION['error'] = "Agent does not exist in the database.";
                return FALSE;
            }
        }catch(PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return FALSE;
        }
    }

    /**
     * Overrides the default __toString()
     * 
     * @return string
     */
    public function __toString(){

        $output = "Name: " . $this->firstname;
        $output .= "<br>";
        $output .= "License Number: " . $this->license_number;

        return $output;
    }
}
?>