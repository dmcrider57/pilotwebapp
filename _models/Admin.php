<?php
/**
 * Fields on the Admin table:
 * 
 * id           int(11)
 * profile      varchar(100)
 * firstname    varchar(50)
 * lastname     varchar(50)
 * username     varchar(25)
 * email        varchar(100)
 * is_active    boolean
 * created_on   datetime
 */
class Admin{
    public $id;
    public $profile;
    public $firstname;
    public $lastname;
    public $username;
    public $email;
    public $is_active;
    public $created_on;

    public function __construct($id, $profile, $fname, $lname, $uname, $email, $active, $created){
        $this->id = $id;
        $this->profile = $profile;
        $this->firstname = $fname;
        $this->lastname = $lname;
        $this->username = $uname;
        $this->email = $email;
        $this->is_active = $active;
        $this->created_on = $created;
    }

    /**
     * Function to login as an admin
     * 
     * @param string $key is the user's unique adminkey
     * @param string $pass is the user's password
     * @param string $uname is the user's username
     * 
     * @return bool TRUE if login was successful, else FALSE
     */
    public static function login($key, $pass, $uname){
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM admin WHERE (username=:uname AND adminkey=:adminkey)";
            $stmt = $db->prepare($command);
            $query = $stmt->execute(array(":uname"=>$uname, ":adminkey"=>$key));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            if($result !== FALSE){
                $storedPassword = $result['password'];
                $hash = password_verify($pass, $storedPassword);

                if($hash){
                    // Successfull login
                    if($result['profile'] === NULL){
                        // Set the default profile image if not set
                        $result['profile'] = '_public/img/profile/default_profile.jpeg';
                    }

                    $_SESSION['current_user'] = new Admin($result['id'], $result['profile'], $result['firstname'], $result['lastname'], $result['username'], $result['email'], $result['is_active'], $result['created_on']);
                    $_SESSION['username'] = $result['username'];
                    $_SESSION['type'] = 'admin';
                    return TRUE;
                }else{
                    $_SESSION['error'] = "Invalid password.";
                    return FALSE;
                }
            }else{
                $_SESSION['error'] = "Invalid username or Admin Key.";
                return FALSE;
            }
        } catch(PDOException $e){
            // Database error
            $_SESSION['error'] = "Database Error: " . $e;
            return FALSE;
        } 
    }

    /**
     * Counts and returns the total number of rows on the $table. There's probably
     * a more efficient way to do this.
     * 
     * @param string $table is the name of the table we need to query
     * 
     * @return int $counter is the number of rows in the table
     */
    public static function getRegistered($table){
        $db = DB::getInstance();
        $command = "SELECT * FROM $table";
        $stmt = $db->prepare($command);
        $query = $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($result !== FALSE){
            $counter = 0;
            foreach($result as $user){
                $counter++;
            }
            return $counter;
        }
    }

    /**
     * Returns the number of active entires on the $table
     * 
     * @param string $table is the name of the table we need to query
     * 
     * @return int $counter is the number of rows where is_active == TRUE
     */
    public static function getActive($table){
        $counter = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM $table WHERE is_active=1";
            $stmt = $db->prepare($command);
            $query = $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($result !== FALSE){
                $counter = 0;
                foreach($result as $user){
                    $counter++;
                }
                return $counter;
            }
        }catch (PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $counter;
        }
    }

    /**
     * Queries the 'client' table and returns an array of Client objects
     * 
     * @return array $clientArray of Client objects
     */
    public static function getClients(){
        $clientArray = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM client";
            $stmt = $db->prepare($command);
            $query = $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($result !== FALSE){
                $clientArray = array();
                foreach($result as $client){
                    $userArray = array($client['id'], $client['firstname'], $client['lastname'], $client['email'], $client['is_active'], $client['created_on'], $client['profile']);
                    array_push($clientArray, new Client($userArray,$client['assigned_agent']));
                }
                return $clientArray;
            }
        }catch (PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $clientArray;
        }
    }

    /**
     * Queries the 'agent' table and returns an array of Agent objects
     * 
     * @return array $agentArray of Agent objects
     */
    public static function getAgents(){
        $agentArray = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agent";
            $stmt = $db->prepare($command);
            $query = $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($result !== FALSE){
                $agentArray = array();
                foreach($result as $agent){
                    $userArray = array($agent['id'], $agent['firstname'], $agent['lastname'], $agent['email'], $agent['is_active'], $agent['created_on'], $agent['profile']);
                    array_push($agentArray, new Agent($userArray,$agent['license_number'], $agent['agency_license']));
                }
                return $agentArray;
            }
        }catch (PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $agentArray;
        }
    }

    /**
     * Queries the 'agency' table and returns an array of Agency objects
     * 
     * @return array $agencyArray of Agency objects
     */
    public static function getAgencies(){
        $agencyArray = NULL;
        try{
            $db = DB::getInstance();
            $command = "SELECT * FROM agency";
            $stmt = $db->prepare($command);
            $query = $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($result !== FALSE){
                $agencyArray = array();
                foreach($result as $agency){
                    array_push($agencyArray, new Agency($agency['id'],$agency['business_name'],$agency['address_street'],$agency['address_city'],$agency['address_state'],$agency['address_zip'],$agency['designated_broker'],$agency['license_number'],$agency['license_type'],$agency['license_status'],$agency['phone_number'],$agency['subscription_status'],$agency['is_active'],$agency['created_on']));
                }
                return $agencyArray;
            }
        }catch (PDOException $e){
            $_SESSION['error'] = "Database error: " . $e;
            return $agencyArray;
        }
    }
}
?>