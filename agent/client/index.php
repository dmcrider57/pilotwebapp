<?php
	if(!isset($_SESSION['username']) or $_SESSION['type'] != 'agent'){
		header('Location: index.php?action=login');
		exit;
	}

	/**
	 * Get the client's information
	 */
	if(isset($_GET['id'])){
		$client = Client::getClient($_GET['id']);
		// 0=id, 1=profile link, 2=firstname, 3=lastname, 4=email, 6=assigned_agent, 7=is_active, 8=created_on

		if(!$client[1]){
			$client[1] = '_public/img/profile/default_profile.jpeg';
		}
	}
	
?>
<?php if(isset($_GET['id'])) : ?>
	<div class="row">
		<div class="col-md-6">
			<div class="profile-content">
				<img src="<?php echo $client[1]; ?>" alt="Profile Image">
				<h3>
					<?php
						echo $client[2] . " " . $client[3];	// Firstname Lastname				
					?>
				</h3>
			</div>
		</div>
		<div class="col-md-6"></div>
	</div>
<?php else : ?>

<?php endif; ?>