<?php
	if(!isset($_SESSION['username']) or $_SESSION['type'] != 'admin'){
		// Redirect if user is not supposed to be here
		header('Location: index.php?action=login');
		exit;
	}

	// Get the data to populate the table below
	$regClients = Admin::getRegistered('client');
	$regAgents = Admin::getRegistered('agent');
	$regAgencies = Admin::getRegistered('agency');
	
	$activeClients = Admin::getActive('client');
	$activeAgents = Admin::getActive('agent');
	$activeAgencies = Admin::getActive('agency');
?>
<table class="table table-hover center">
	<thead class="table-header">
		<tr>
			<td></td>
			<td>Clients</td>
			<td>Agents</td>
			<td>Agencies</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="table-label">Total # Registered:</td>
			<td><?php echo $regClients; ?></td>
			<td><?php echo $regAgents; ?></td>
			<td><?php echo $regAgencies; ?></td>
		</tr>
		<tr>
			<td class="table-label">Total # Active:</td>
			<td><?php echo $activeClients; ?></td>
			<td><?php echo $activeAgents; ?></td>
			<td><?php echo $activeAgencies; ?></td>
		</tr>
		<tr>
			<td></td>
			<td><a href="index.php?action=adminEdit&view=client" class="btn btn-lg btn-primary btn-block">View</a></td>
			<td><a href="index.php?action=adminEdit&view=agent" class="btn btn-lg btn-primary btn-block">View</a></td>
			<td><a href="index.php?action=adminEdit&view=agency" class="btn btn-lg btn-primary btn-block">View</a></td>
		</tr>
	</tbody>
</table>