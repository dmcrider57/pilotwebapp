<?php
	// Check if form is submitted
	function addUser(){
		if(isset($_POST['username']) and isset($_POST['adminkey']) and isset($_POST['password'])){
			// Get all the fields
			$firstname = stripslashes($_POST['firstname']);
			$lastname = stripslashes($_POST['lastname']);
			$password = $_POST['password'];
			$password2 = $_POST['password2'];
            $email = $_POST['email'];
            $username = $_POST['username'];
            $adminkey = $_POST['adminkey'];
			
			if($password === $password2 && filter_var($email, FILTER_VALIDATE_EMAIL)){
				
				// Hash the password for security
				$hash = password_hash($password, PASSWORD_DEFAULT);
				
				try{
					// Initiate a connection to the database
					$db = DB::getInstance();
					$command = "INSERT INTO admin(firstname, lastname, username, adminkey, email, password, created_on) VALUES (:firstname, :lastname, :username, :adminkey, :email, :password, NOW());";
                    $query = $db->prepare($command);
                    $result = $query->execute(array(":firstname" => $firstname, ":lastname" => $lastname, ":username"=>$username, ":adminkey"=>$adminkey, ":email" => $email, ":password" => $hash));
					
                    Admin::login($adminkey, $password, $username);
					header("Location: index.php?action=admin");
					exit;
				} catch (PDOException $e){
					// Database error
                    $_SESSION['error'] = "PDO Error: " . $e;
                    header("Location: index.php?action=adminRegister");
					exit;
				}
			}else{
				// Passwords don't match, or email is not valid
                $_SESSION['error'] = "Something went wrong. Please try again later.";
                header("Location: index.php?action=adminRegister");
                exit;
			}
		}else{
			// One of username, adminkey, or password is not set
            $_SESSION['error'] = "Something went wrong. Please try again later.";
            header("Location: index.php?action=adminRegister");
            exit;
		}
	}

	function doesUserExist($user, $key){
		try{
			$db = DB::getInstance();
			$command = "SELECT * FROM admin WHERE (username=:username AND adminkey=:adminkey)";
			$stmt = $db->prepare($command);
			$query = $stmt->execute(array(":username"=>$user, ":adminkey"=>$key));
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if($result !== FALSE){
				// User exists
				return TRUE;
			}else{
				// User does not exist - this is what we want
				return FALSE;
			}
		}catch(PDOException $e){
			// Database error
			$_SESSION['error'] = "PDO Error: " . $e;
			return FALSE;
		}

		if(!isset($users[0])){
			// No user with that username exists
			return TRUE;
		}else{
			// Username already exists - cannot add user
			$_SESSION['error'] = "An account with those credentials already exists.";
			return FALSE;
		}
    }
    
    function checkKey($key, $username){
		try{
			$db = DB::getInstance();
			$command = "SELECT * from adminkey WHERE adminkey=:adminkey";
			$stmt = $db->prepare($command);
			$query = $stmt->execute(array(":adminkey"=>$key));
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if($result !== FALSE){
				// Key was found - it's a valid key
				if($result['is_active'] == TRUE){
					// The key is active
					if($result['admin_username'] == $username){
						// Admin key matches username
						return TRUE;
					}else{
						// Admin key and username do not match
						$_SESSION['error'] = "Usernames do not match";
						return FALSE;
					}
				}else{
					// The key is not active
					$_SESSION['error'] = "Key not active";  
					return FALSE;
				}
			}else{
				// Not a valid key
				$_SESSION['error'] = "Key not valid";
				return FALSE;
			}
		}catch(PDOException $e){
			// Database error
			$_SESSION['error'] = "PDO Error: " . $e;
			return FALSE;
		}
	}
	
	if(isset($_POST['adminkey']) and isset($_POST['register'])){
		if(!doesUserExist($_POST['username'], $_POST['adminkey'])){
            if(checkKey($_POST['adminkey'], $_POST['username'])){
                addUser();
            }else{
				// Invalid key
				$_SESSION['error'] .= "<br>Invalid adminkey<br>AdminKey: " . $_POST['adminkey'] . "<br>Username: " . $_POST['username'];
				header("Location: index.php?action=adminRegister");
				exit;
			}
		}else{
			// User already exist
			$_SESSION['error'] .= "<br>User already exists";
			header("Location: index.php?action=adminRegister");
			exit;
		}
	}
?>
<!-- Registration form -->
<form class="form-sign-in" method="POST" action="">
	<h2 class="form-sign-in-header">Admin Registration</h2>

	<?php if(isset($_SESSION['result'])) : ?>
		<div id="error-message"><?php echo $_SESSION['result'] ?></div>
	<?php endif; ?>
	<?php unset($_SESSION['result']); ?>

	<?php if(isset($_SESSION['error'])) : ?>
		<div id="error-message"><?php echo $_SESSION['error'] ?></div>
	<?php endif; ?>
	<?php unset($_SESSION['error']); ?>
	
	<input type="text" name="firstname" class="form-control" placeholder="First name" required>
	<input type="text" name="lastname" class="form-control" placeholder="Last name" required>
    <input type="text" name="username" class="form-control" placeholder="Username" required>
	
	<input type="email" name="email" class="form-control" placeholder="Email" required>
	
	<input type="text" name="adminkey" class="form-control" placeholder="Admin Key" required>

	<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

	<input type="password" name="password2" id="verifyPassword" class="form-control" placeholder="Verify Password" required>

	<button class="btn btn-lg btn-primary btn-block" type="submit" name="register">Register</button>
	<a class="btn btn-lg btn-primary btn-block" href="index.php?action=adminLogin">Login</a>
</form>