<?php
	/**
	 * Once the user clicks the 'Login' button, verify they entered information
	 * and attempt to login using the appropriate class.
	 */
	if(isset($_POST['adminkey']) and isset($_POST['password']) and isset($_POST['username']) and isset($_POST['login'])){
		if(Admin::login($_POST['adminkey'], $_POST['password'], $_POST['username'])){
            header("Location: index.php?action=admin");
            exit;
        }else{
            // Refresh to show any error messages
            header("Location: index.php?action=adminLogin");
            exit;
        }
	}
?>
<?php
	/**
	 * Display the update message if the user successfully
	 * updated their password
	 */
	if(isset($_SESSION["update_message"])){
		echo "<p id='success-message'>" . $_SESSION["update_message"] . "</p>";
		unset($_SESSION['update_message']);
	}
?>
<!-- Login Form -->
<form class="form-sign-in" method="POST" action="">
	<h2 class="form-sign-in-header">Admin Login</h2>

	<?php if(isset($_SESSION['error'])) : ?>
		<div id="error-message"><?php echo $_SESSION['error'] ?></div>
	<?php endif; ?>
	<?php unset($_SESSION['error']); ?>

	<input type="text" name="adminkey" class="form-control" placeholder="Admin Key" required>
    <input type="text" name="username" class="form-control" placeholder="Username" required>
	
	<label for="inputPassword" class="sr-only">Password</label>
	<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

	<button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Login</button>
    <a class="btn btn-lg btn-primary btn-block" href="index.php?action=adminRegister">Register</a>
</form>
<!-- End Login Form-->