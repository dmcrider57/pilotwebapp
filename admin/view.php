<?php
  /**
   * TODO: Make License Status, Sunscription Status and Is Active dropdown fields so I don't have to keep converting values to text
   */
  if(!isset($_SESSION['username']) or $_SESSION['type'] != 'admin' or !isset($_GET['view'])){
  // Redirect if user is not supposed to be here
  header('Location: index.php?action=login');
  exit;
  }
  
  $content = $_GET['view'];
  
  // Determine which information to display
  if($content == 'client'){
    $userArray = Admin::getClients();
  }else if ($content == 'agent'){
    $userArray = Admin::getAgents();
  }else if ($content == 'agency'){
    $userArray = Admin::getAgencies();
  }

  // Listen for the AJAX Call
  if(Isset($_POST['id'])){
    try{
        $db = DB::getInstance();
        $user = $_POST['id'];
        $field = $_POST['field'];
        $value = $_POST['value'];
        $table = $_POST['table'];

        $command = "UPDATE ".$table." SET ".$field."='".$value."' WHERE id=".$user;
        $stmt = $db->prepare($command);
        $query = $stmt->execute();

        echo $query;

    }catch(PDOException $e){
        echo "Database error: " . $e;
    }
  }
?>
<script>
  $(function(){
    var message_status = $("#status");
    
    $("td[contenteditable=true]").blur(function(){
      var field_id = $(this).attr("id");
      var split_id = field_id.split(":");
      var user_id = split_id[0];
      var field_name = split_id[1];
      var value = $(this).text();
      var table_name = "<?php echo $content; ?>";

      
      // Need to find a way to only make this call if the field was actually changed.
      $.ajax({
        url:'index.php?action=adminEdit&view=<?php echo $content; ?>',
        type:'post',
        data:{'id':user_id, 'field':field_name,'value':value,'table':table_name},
        success:function(response){
          console.log("Success");
        },
        error:function(){
          console.log("Error");
        }
      });
    });
  });
</script>
<div class="info">
  <p>Select any highlighted field and update the information. Moving away from a field (i.e. tabbing or clicking away) will automatically save the changes.</p>
</div>
<table class="table">
  <thead>
    <tr class="table-header">
      <?php if ($content != 'agency') : ?>
        <td>Profile Image</td>
        <td>ID</td>
        <td>Firstname</td>
        <td>Lastname</td>
        <td>Email</td>
      <?php endif; ?>
      <?php if($content == 'client') : ?>
        <td>Assigned Agent</td>
      <?php endif; ?>
      <?php if($content == 'agent') : ?>
        <td>License Number</td>
        <td>Agency License</td>
      <?php endif; ?>
      <?php if($content == 'agency') : ?>
        <td>Business Name</td>
        <td>Street</td>
        <td>City</td>
        <td>State</td>
        <td>Zip</td>
        <td>Phone Number</td>
        <td>Designated Broker</td>
        <td>License Number</td>
        <td>License Type</td>
        <td>License Status</td>
        <td>Subscription Status</td>
      <?php endif; ?>
      <td>Is Active</td>
      <td>Created On</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach($userArray as $user) : ?>
    <?php
      switch($user->is_active){
        case 0:
          $is_active = "Inactive";
          break;
        case 1:
          $is_active = "Active";
          break;
      }
      if($content == 'agency'){
        switch($user->license_status){
          case 0:
            $license_status = "Inactive";
            break;
          case 1:
            $license_status = "Active";
            break;
        }
  
        switch($user->subscription_status){
          case 0:
            $subscription_status = "Not Active";
            break;
          case 1:
            $subscription_status = "3 Month Trial";
            break;
          case 2:
            $subscription_status = "Contract - 1 Year";
            break;
          case 3:
            $subscription_status = "Contract - 3 Year";
            break;
        }
      }
    ?>
      <tr>
        <?php if ($content != 'agency') : ?>
          <td><a href="<?php if($user->profile === NULL){echo '_public/img/profile/default_profile.jpeg';}else{echo $user->profile;} ?>"><img src="<?php if($user->profile === NULL){echo '_public/img/profile/default_profile.jpeg';}else{echo $user->profile;} ?>" /></a></td>
          <td><?php echo $user->id; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:firstname"><?php echo $user->firstname; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:lastname"><?php echo $user->lastname; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:email"><?php echo $user->email; ?></td>
        <?php endif; ?>
        <?php if($content == 'client') : ?>
          <td contenteditable="true" id="<?php echo $user->id; ?>:assigned_agent"><?php echo $user->assigned_agent; ?></td>
        <?php endif; ?>
        <?php if($content == 'agent') : ?>
          <td contenteditable="true" id="<?php echo $user->id; ?>:license_number"><?php echo $user->license_number; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:agency_license"><?php echo $user->agency_license; ?></td>
        <?php endif; ?>
        <?php if($content == 'agency') : ?>
          <td contenteditable="true" id="<?php echo $user->id; ?>:business_name"><?php echo $user->business_name; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:address_street"><?php echo $user->address_street; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:address_city"><?php echo $user->address_city; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:address_state"><?php echo $user->address_state; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:address_zip"><?php echo $user->address_zip; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:phone_number"><?php echo $user->phone_number; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:designated_broker"><?php echo $user->designated_broker; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:license_number"><?php echo $user->license_number; ?></td>
          <td contenteditable="true" id="<?php echo $user->id; ?>:license_type"><?php echo $user->license_type; ?></td>
          <td><?php echo $license_status; ?></td>
          <td><?php echo $subscription_status; ?></td>
        <?php endif; ?>
        <td><?php echo $is_active; ?></td>
        <td><?php echo date('Y-M-d',strtotime($user->created_on)); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>