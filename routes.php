<?php
    function call($action){
        require_once('_controllers/pages_controller.php');
		$controller = new PagesController();
        if($action == 'pages'){
            $controller->{'home'}();
        }else{
            $controller->{$action}();
        }
    }

    $controllers = array('pages' => ['home','login','logout','register','error','agent','client','account','editUser','agency', 'editAgents', 'editAgency', 'showClient','editAgent','admin','adminLogin','adminRegister','adminEdit']);

    if(in_array($action, $controllers['pages'])){
        call($action);
    }else{
        call('error');
    }
?>