<?php
    if(isset($_SESSION['db_error'])){
        echo $_SESSION['db_error'];
    }
    unset($_SESSION['db_error']);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pilot</title>
        <link rel="stylesheet" href="_public/css/bootstrap.min.css">
        <link rel="stylesheet" href="_public/css/custom.css">
        <!--Scripts go here-->
        <script src="_public/js/jquery.min.js"></script>
        <script src="_public/js/popper.min.js"></script>
        <script src="_public/js/bootstrap.min.js"></script>
    </head>
    <body class="container-fluid">
        <div class="row top-row">
            <div class="col-md-12">
                <?php require_once('_partials/nav.php'); ?>
            </div>
        </div>
        <div class="row">
            <?php if(isset($_SESSION['username']) and (isset($_GET['action']) and ($_GET['action'] != 'account' and $_GET['action'] != 'editUser'))) : ?>
                <div class="col-md-2">
                    <?php
                        if($_SESSION['type'] == 'agent'){
                            require_once('_partials/agent_sidebar_left.php');
                        }else if($_SESSION['type'] == 'client'){
                            require_once('_partials/client_sidebar_left.php');
                        }else if($_SESSION['type'] == 'admin'){
                            require_once('_partials/admin_sidebar_left.php');
                        }
                    ?>
                </div>
            <?php else : ?>
                <div class="col-md-2"></div>
            <?php endif; ?>
            <div class="col-md-8 panel">
                <?php require_once('routes.php'); ?>
            </div>
            <?php if(isset($_SESSION['username']) and (isset($_GET['action']) and ($_GET['action'] != 'account' and $_GET['action'] != 'editUser'))) : ?>
                <div class="col-md-2">
                    <?php
                        if($_SESSION['type'] == 'agent'){
                            require_once('_partials/agent_sidebar_right.php');
                        }else if($_SESSION['type'] == 'client'){
                            require_once('_partials/client_sidebar_right.php');
                        }else if($_SESSION['type'] == 'admin'){
                            require_once('_partials/admin_sidebar_right.php');
                        }
                    ?>
                </div>
            <?php else : ?>
                <div class="col-md-2"></div>
            <?php endif; ?>
        </div>
        <div class="row">
            <footer>
                <p class="text-muted">Copyright &copy 2018 Daylon Crider</p>
            </footer>
        </div>
    </body>
</html>