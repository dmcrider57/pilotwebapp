<?php
    /**
     * This is the content for the heading of the website.
     * It contains 3 parts:
     *  - The Pilot company logo on the left of the page
     *  - The Pilot name and slogan centered on the page
     *  - The navigation menu (shown only when the user is logged in) on the right of the page
     */
    if(isset($_SESSION['type'])){
        $redirect = $_SESSION['type'];
    }else{
        $redirect = "login";
    }
?>
<div class="row">
    <div class="col-md-4 brand">
        <a href="index.php?action=<?php echo $redirect; ?>"><img class="header-logo" src="_public/img/logo/generic-header.jpeg"></a>
    </div>
    <div class="col-md-4 header">
        <h1>Pilot</h1>
        <h3>Today's Commerical Real Estate Platform</h3> 
    </div>
    <?php  // Only show the naviagation menu if the user is logged in ?>
    <?php if(isset($_SESSION['username'])) : ?>
        <div class="col-md-4 user">
            <button type="button" class="btn btn-primary outline dropdown-toggle" data-toggle="dropdown">
                <img src="_public/img/glyphicons/glyphicons-4-user.png">
                <?php echo $_SESSION['username'] ?>
            </button>
            <ul class="dropdown-menu">
                <?php if(isset($_GET['action'])) : ?>
                    <?php if($_GET['action'] != '' and $_GET['action'] != $_SESSION['type']) :  ?>
                        <li class="dropdown-item"><a href="index.php?action=<?php echo $_SESSION['type']; ?>"><img src="_public/img/glyphicons/glyphicons-21-home.png"> Home</a></li>
                    <?php endif; ?>
                <?php endif; ?>
                <?php // Only show this option if the user is a Designated Broker ?>
                <?php if(isset($_SESSION['is_designated_broker'])) : ?>
                    <?php if($_SESSION['is_designated_broker'] === TRUE) : ?>
                        <li class="dropdown-item"><a href="index.php?action=agency"><img src="_public/img/glyphicons/glyphicons-44-group.png"> Manage Agency</a></li>
                    <?php endif; ?>
                <?php endif; ?>
                <li class="dropdown-item"><a href="index.php?action=account"><img src="_public/img/glyphicons/glyphicons-265-vcard.png"> My Account</a></li>
                <li class="dropdown-item"><a href="https://www.docusign.com"><img src="_public/img/glyphicons/glyphicons-236-pen.png"> DocuSign</a></li>
                <li class="dropdown-item"><a href="index.php?action=logout"><img src="_public/img/glyphicons/glyphicons-388-log-out.png"> Logout</a></li>
            </ul>
        </div>
    <?php else : ?>
        <div class="col-md-4"></div>
    <?php endif; ?>
</div>