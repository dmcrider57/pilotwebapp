<?php
    /**
     * This content is placed on the left side of the screen.
     * It is only viewable by agents.
     */
?>
<!-- Tab Headers -->
<div class="tab">
    <button class="tablinks" onclick="openTab(event, 'Clients')" id="defaultOpen">Clients</button>
    <button class="tablinks" onclick="openTab(event, 'Listings')">Listings</button>
</div>
<!-- Tab conent -->
<div id="Clients" class="tabcontent">
    <!-- Loop through each client and list them -->
    <?php
        $clients = Agent::getClients($_SESSION['current_user']);
    ?>
    <?php if($clients !== NULL) : ?>
        <?php foreach($clients as $client) : ?>
            <p><a href="index.php?action=showClient&id=<?php echo $client[0]; ?>"><?php echo $client[1]; ?></a></p>
        <?php endforeach; ?>
    <?php else : ?>
        <p>You don't have any Clients yet.</p>
    <?php endif; ?>
</div>

<div id="Listings" class="tabcontent">
    <!-- Loop through each client and list them -->
    <p>Great Listing #1</p>
</div>
<!-- Script for tabs-->
<script>
    // Open the default tab
    document.getElementById("defaultOpen").click();

    function openTab(evt, tabName) {
        var i, tabcontent, tablinks;

        // Get all elements 'tabcontent' and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for(i = 0; i < tabcontent.length; i++){
            tabcontent[i].style.display = "none";
        }

        // Get all elements 'tablinks' and remove 'active'
        tablinks = document.getElementsByClassName("tablinks");
        for(i = 0; i < tablinks.length; i++){
            tablinks[i].className = tablinks[i].className.replace(" active","");
        }

        // Show current tab and add 'active'
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>