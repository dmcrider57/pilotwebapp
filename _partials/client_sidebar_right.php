<?php
    /**
     * This content is placed on the right side of the screen.
     * It is only viewable by clients.
     */
?>
<!-- Tab Headers -->
<div class="tab-user">
    <button class="userlinks" onclick="openLink(event, 'Chat')" id="default">Agent</button>
    <button class="userlinks" onclick="openLink(event, 'Documents')">Documents</button>
</div>
<!-- Tab conent -->
<div id="Chat" class="user-content">
    <!-- Loop through each client and list them -->
    <p>Chat with Agent Steve</p>
</div>

<div id="Documents" class="user-content">
    <!-- Loop through each client and list them -->
    <p>Document #1 - Lease Agreement</p>
</div>
<!-- Script for tabs-->
<script>
    // Open the default tab
    document.getElementById("default").click();

    function openLink(evt, tabName) {
        var i, tabcontent, tablinks;

        // Get all elements 'tabcontent' and hide them
        tabcontent = document.getElementsByClassName("user-content");
        for(i = 0; i < tabcontent.length; i++){
            tabcontent[i].style.display = "none";
        }

        // Get all elements 'tablinks' and remove 'active'
        tablinks = document.getElementsByClassName("userlinks");
        for(i = 0; i < tablinks.length; i++){
            tablinks[i].className = tablinks[i].className.replace(" active","");
        }

        // Show current tab and add 'active'
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>