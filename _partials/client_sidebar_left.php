<?php
    /**
     * This content is placed on the left side of the screen.
     * It is only viewable by clients.
     */
?>
<!-- Tab Headers -->
<div class="tab">
    <!--<button class="tablinks" onclick="openTab(event, 'Agents')" id="defaultOpen">Agents</button>-->
    <button class="tablinks" onclick="openTab(event, 'Listings')" id="defaultOpen">Listings</button>
</div>
<!-- Tab conent -->
<!--
<div id="Agents" class="tabcontent">
    Loop through each client and list them
    <p>Steve Client</p>
    <p>Client Tom</p>
</div>
-->

<div id="Listings" class="tabcontent">
    <!-- Loop through each client and list them -->
    <p>Great Listing #1</p>
</div>
<!-- Script for tabs-->
<script>
    // Open the default tab
    document.getElementById("defaultOpen").click();

    function openTab(evt, tabName) {
        var i, tabcontent, tablinks;

        // Get all elements 'tabcontent' and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for(i = 0; i < tabcontent.length; i++){
            tabcontent[i].style.display = "none";
        }

        // Get all elements 'tablinks' and remove 'active'
        tablinks = document.getElementsByClassName("tablinks");
        for(i = 0; i < tablinks.length; i++){
            tablinks[i].className = tablinks[i].className.replace(" active","");
        }

        // Show current tab and add 'active'
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>