<?php
    $purch_price = "$100,000";
    $rent_price = "$5,850";
    $utilities_cost = "$360";
?>
<div class="row">
    <div class="col-md-6">
        <h3>Current Offers</h3>
        <ol>
            <!-- Loop and list each offer-->
            <li><a href="#" id="o1">Offer 1</a></li>
            <li><a href="#" id="o2">Offer 2</a></li>
            <li><a href="#" id="o3">Offer 3</a></li>
        </ol>
    </div>
    <div class="col-md-6">
        <h3>Potential Listings</h3>
        <ol>
            <!-- Loop and list each offer-->
            <li><a href="#" id="l1">Listing 1</a></li>
            <li><a href="#" id="l2">Listing 2</a></li>
            <li><a href="#" id="l3">Listing 3</a></li>
        </ol>
    </div>
</div>
<hr>
<div class="row">
    <!-- Need to display details based on what was clicked above-->
    <div class="col-md-6">
        <h2>Listing 2</h2>
        <p>Purchase Price: <?php echo $purch_price ?></p>
        <p>Rental Price: <?php echo $rent_price ?></p>
        <p>Average Utilities: <?php echo $utilities_cost ?></p>
    </div>
    <div class="col-md-6">
        Map goes here
    </div>
</div>