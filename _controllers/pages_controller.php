<?php
    class PagesController{
        public function home(){
            require_once('home.php');
        }

        public function error(){
            require_once('_partials/error.php');
        }

        public function login(){
            require_once('login/index.php');
        }

        public function logout(){
            require_once('logout/index.php');
        }

        public function register(){
            require_once('register/index.php');
        }
		
		public function agent(){
			require_once('agent/index.php');
		}
		
		public function client(){
			require_once('client/index.php');
		}
		
		public function account(){
			require_once('account/index.php');
		}
		
		public function admin(){
			require_once('admin/index.php');
        }

        public function adminLogin(){
            require_once('admin/login.php');
        }

        public function adminRegister(){
            require_once('admin/register.php');
        }

        public function adminEdit(){
            require_once('admin/view.php');
        }
        
        public function editUser(){
            require_once('account/edit/index.php');
        }

        public function agency(){
            require_once('account/agency/index.php');
        }

        public function editAgents(){
            require_once('account/agency/edit/agents/index.php');
        }

        public function editAgency(){
            require_once('account/agency/edit/index.php');
        }

        public function showClient(){
            require_once('agent/client/index.php');
        }
    }
?>